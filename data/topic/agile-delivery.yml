title: What is Agile delivery?
seo_title: What is Agile delivery?
description: Agile delivery is an iterative approach to software delivery in
  which teams build software incrementally at the beginning of a project rather
  than shipping it all at once upon completion.
header_body: >-
  Just as Agile project management brings an iterative approach to project
  management, Agile delivery is an iterative approach to software delivery in
  which teams build software incrementally at the beginning of a project rather
  than shipping it all at once upon completion.


  [Learn how to accelerate delivery →](https://learn.gitlab.com/agile-topic/201906-whitepaper-re#page=1){:data-ga-name="Accelerate delivery"}{:data-ga-location="header"}


canonical_path: /topics/agile-delivery/
file_name: agile-delivery
twitter_image: /images/opengraph/ci-cd-opengraph.png
related_content:
  - title: GitLab's ultimate guide to Agile project & portfolio management (PPM)
    url: https://about.gitlab.com/topics/agile-delivery/agile-ppm/
cover_image: /images/topics/agile.svg
body: >-
  ## What are Agile processes?


  Agile is one of the most important and transformative methodologies introduced to the software engineering discipline in recent decades, helping software teams efficiently create customer-centric products.


  Agile development means taking iterative, incremental, and lean approaches to streamline and accelerate the delivery of projects. It is based on the Agile Manifesto, which values individuals and interactions, working software, customer collaboration, and responding to change.


  Agile project management processes are an alternative to traditional project management approaches such as the Waterfall or Spiral methodologies, which assume a linear path from one stage to another. Agile brings this to the next level by empowering teams to not only accept but embrace a non-linear workflow that changes and evolves during the development process. Agile processes emphasize building working products quickly and iteratively, and focus on empowering cross-functional teams rather than establishing top-down hierarchies.


  ## The key principles of Agile software delivery


  By rapidly responding to changes and adapting their plans accordingly, Agile delivery teams are able to deliver high-quality products and services quickly and efficiently. These benefits are achieved through the application of the four key principles of Agile delivery:


  1. **Collaboration**: Agile methodologies value people and human interactions over processes and tools. Agile approaches help teams keep the focus on team members by allowing communication to happen in a fluid, natural way as the need arises. And when team members can communicate freely and naturally, they can collaborate more effectively.

  2. **Customer focus**: The highest priority of Agile teams, as outlined in the Agile Manifesto, is “to satisfy the customer through early and continuous delivery of valuable software.” In other words, it all comes down to delivering better products to customers more quickly.

  3. **Value-based prioritization**: The process of prioritization — determining what should be done now and what can be done later — is a core principle of the Scrum methodology, a popular agile framework. Prioritization allows teams to deliver the most possible value to customers in the shortest amount of time.

  4. **Iterative development**: In Agile delivery, tasks are broken down into smaller deliverables that can be repeated and refined throughout the software development cycle. This allows teams to constantly review their progress and identify opportunities for improvement.


  ## Agile delivery basics and benefits


  ### Agile mindset


  An Agile mindset means viewing setbacks as learning opportunities, embracing iteration, collaboration, and change, and focusing on delivering value. With an Agile mindset, teams can adjust to changing market needs, respond to customer feedback, and deliver business value. Adopting a new perspective can positively change a team’s culture, since the shift permits innovation without fear, collaboration with ease, and delivery without roadblocks.


  ### Agile environment


  An Agile environment is a workplace that is designed to support Agile processes. Agile environments favor individuals and interactions over processes and tools, working software over comprehensive documentation, customer collaboration over contract negotiation, and responding to change over following a plan. An Agile environment encourages team members to work collaboratively and promotes constant innovation and process improvement.


  ### Speed to market


  Faster time to market enables quicker customer feedback and higher customer satisfaction.


  ### Higher quality


  Since testing is integrated throughout the lifecycle, teams have early sight into quality issues.


  ### Transparency


  Teams are involved throughout a project — from planning and prioritizing to building and deploying.


  ## Agile delivery frameworks


  There are many different agile delivery frameworks, but some of the most common are Scrum, Kanban, and Lean. Each of these frameworks has its own unique set of values, principles, and practices that help guide organizations in their transition to an agile way of working.


  ### Scrum


  Scrum, often synonymous with Agile, is an approach that emphasizes continuous improvement, self organization, and experience-based learning. By utilizing user stories, tasks, backlogs, and extensions, teams have a structured model to carry them across a software development lifecycle. Teams that use a Scrum approach to development are likely to be committed, respectful, and focused.


  ### Kanban


  Teams that use a Kanban framework favor transparency and communication. Tasks are organized using Kanban cards on a board to enable end-to-end visibility throughout production. Three practices guide Kanban: visualize work, limit work in progress, and manage flow. Teams that use a Kanban framework are collaborative, transparent, balanced, and customer focused.


  ### Lean


  Lean software development comes from lean manufacturing principles and practices and focuses on eliminating waste, amplifying learning, deciding as late as possible, delivering as fast as possible, empowering the team, building integrity in, and optimizing the whole.


  ## What makes Agile software delivery so effective?


  There are a number of reasons why Agile software delivery methods are becoming increasingly popular. First, they allow for much more flexibility and responsiveness to change than traditional waterfall methods. Organizations that are successful with Agile software delivery methods have clear business priorities and engage users and feedback in active delivery refinements.


  Core Agile methodology elements within the software delivery process help make it a success.


  * Teams are kept small and iterations short

  * Feedback from customers is fast

  * Business priorities are value-based

  * Users are engaged in the refining of end-product requirements


  Agile methods tend to focus on delivering value to the end user, rather than simply meeting internal deadlines or milestones. Value-based business priorities and engaging users in refining requirements are key to making agile software delivery methods work effectively.


  ## What are common challenges with Agile delivery?


  Agile delivery can be a great way to improve your software development process, but it can also present some challenges. For example, you may need to change the way you communicate with stakeholders, or you may need to adjust your project management approach. You may also find that you need to invest in new tools and training for your team.


  Common challenges with Agile approaches include:


  * Constant feedback and collaboration between the customer and the development team in order to deliver a more flexible and responsive product

  * New tools to manage and the need to integrate the Agile structure and methodology across teams and stakeholders

  * Team members need to be trained in and aware of Agile concepts in order to improve performance and streamline processes


  If you’re willing to overcome these challenges, you’ll likely find that Agile delivery can help you speed up your development process and improve your software quality.


  ## Why embrace Agile processes?


  The demand for faster software development is universal, and [Agile delivery meets both customer and business needs](/solutions/agile-delivery/).


  Organizations that adopt Agile practices can gain a competitive edge in a fast-changing market. Businesses that empower teams to use Agile processes satisfy discerning customers and adapt to new technologies, helping them to develop the products that set the standard for industries.


  It’s not just businesses that benefit from Agile delivery. Customers have more substantive experiences with organizations when their needs are met and their feedback makes a difference in product development. Customers appreciate when their input and expectations help shape an organization’s releases.


  ## How can I get started with Agile software development?


  If you’re looking to [get started with Agile software development](/blog/2018/03/05/gitlab-for-agile-software-development/), there are a few things you can do. First, you’ll need to identify what your Agile software development and delivery process will look like. You’ll also need to consider what your team’s goals are, what your customers’ needs are, and what your delivery timeline looks like.


  Once you have a good understanding of all of these factors, you can begin to put together your Agile development and delivery process. Once structured and implemented, you will then need to monitor your development process to ensure it is working as required, and if not, optimize as necessary.
benefits_title: ""
benefits_description: ""
benefits: []
resources_title: Resources
resources_intro: >
  Here’s a list of resources on Agile that we find to be particularly helpful in
  understanding Agile and implementation. We would love to get your
  recommendations on books, blogs, videos, podcasts and other resources that
  tell a great Agile story or offer valuable insight on the definition or
  implementation of the practice.


  Please share your favorites with us by tweeting us [@GitLab](https://twitter.com/gitlab)!
resources:
  - title: Getting started with GitLab issues
    url: https://learn.gitlab.com/agile-topic/watch-40
    type: Webcast
  - title: Getting started with GitLab epics
    url: https://learn.gitlab.com/agile-topic/watch-41
    type: Webcast
  - title: Scaled Agile Framework (SAFe) with GitLab
    url: https://www.youtube.com/watch?v=PmFFlTH2DQk
    type: webcast
  - title: Agile planning
    url: /solutions/agile-delivery/
    type: Reports
  - title: Scaled Agile and GitLab
    url: https://about.gitlab.com/solutions/agile-delivery/scaled-agile/
    type: Reports
  - title: Accelerating software delivery
    url: /solutions/agile-delivery/scaled-agile/
    type: Reports
  - url: /customers/axway-devops/
    title: Axway was able to achieve hourly deployments with GitLab CI/CD
    type: Case studies
  - title: How GitLab CI supported Ticketmaster's ramp up to weekly mobile releases
    url: /blog/2017/06/07/continuous-integration-ticketmaster/
    type: Case studies
suggested_content:
  - url: /blog/2018/03/05/gitlab-for-agile-software-development/
  - url: /blog/2019/06/13/agile-mindset/
  - url: /blog/2018/08/02/4-ways-to-use-gitlab-issue-boards/
  - url: /blog/2019/06/20/issue-labels-can-now-be-scoped/
  - url: /blog/2019/06/10/manage-conversation-staying-agile/
  - url: /blog/2019/08/20/agile-pairing-sessions/
schema_faq:
  - question: What is Agile delivery?
    answer: Agile delivery is an iterative approach to software delivery in which
      teams build software  incrementally at the beginning of a project rather
      than ship it at once upon completion.
    cta:
      - url: https://about.gitlab.com/topics/agile-delivery/#what-is-agile-delivery:~:text=What%20is%20Agile%20delivery%3F
        text: Learn more about Agile delivery
  - question: Why embrace Agile delivery?
    answer: Businesses that empower teams to use Agile development practices satisfy
      discerning customers and adapt to new technologies, helping them to
      develop the products that set the standard for industries.
    cta:
      - text: Learn more about embracing Agile delivery
        url: https://about.gitlab.com/topics/agile-delivery/#why-embrace-agile-delivery:~:text=Why%20embrace%20Agile%20delivery
